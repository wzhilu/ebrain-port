# ebrainPort

#### 介绍
简易的量化日志分析平台。使用SpringBoot开发后台服务，连接Mysql数据库做业务日志的增删改查操作；使用kafka-clients连接到Kafka系统实时过滤和提取VNPY产生的交易订单信息，并通知到钉钉群；使用HttpClient获取策略易返回的同花顺交易记录并提交到后台数据库做持久化存储；使用MetaBase连接到Mysql数据库，做数据分析和页面展示；使用javax.mail自动发送每天的交易盈利日报邮件；使用quartz管理和调度各个任务模块。

#### 软件架构
软件架构说明


#### 安装教程

1.  xxxx
2.  xxxx
3.  xxxx

#### 使用说明

1.  xxxx
2.  xxxx
3.  xxxx

#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request


#### 特技

1.  使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2.  Gitee 官方博客 [blog.gitee.com](https://blog.gitee.com)
3.  你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解 Gitee 上的优秀开源项目
4.  [GVP](https://gitee.com/gvp) 全称是 Gitee 最有价值开源项目，是综合评定出的优秀开源项目
5.  Gitee 官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6.  Gitee 封面人物是一档用来展示 Gitee 会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
