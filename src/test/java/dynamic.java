import java.util.HashMap;
import java.util.Map;

class dynamic {

    private static int[] array = {1, 2, 5};

    public static void main(String[] args) {
        coinChange(11);

    }

    public static int coinChange(int amount) {
        if (amount == 0) {
            return 0;
        }
        if (amount < 0) {
            return -1;
        }
        int res = 0;
        for (int coin : array) {
            int subProblem = coinChange(amount - coin);
            if (subProblem == -1) {
                continue;
            }
            res = Math.min(res, 1 + subProblem);
        }
        return res;
    }




}