import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

class Didi {

    public static void main(String[] args) {
        System.out.println("result:" + another(20));
        int[] array = {1, 10, 8, 7, -7, 9, 2, 2, 5, 11, 8};
        towSum(array, 10);
    }

    static int fib(int n) {
        if (n < 1) {
            return 0;
        }
        int[] memory = new int[n + 1];
        return helper(memory, n);
    }

    static int helper(int[] memory, int n) {
        if (n == 1 || n == 2) {
            System.out.println("direction:" + n);
            return 1;
        }
        if (memory[n] != 0) {
            return memory[n];
        }
        System.out.println(n);
        memory[n] = helper(memory, n - 1) + helper(memory, n - 2);
        return memory[n];
    }

    static int another(int n) {
        if (n < 1) {
            return 0;
        }
        if (n == 1 || n == 2) {
            return 1;
        }
        int pre = 1, cur = 1;
        for (int i = 3; i <= n; i++) {
            int sum = pre + cur;
            pre = cur;
            cur = sum;
        }
        return cur;
    }

    public static void towSum(int[] array, int amount) {
        Map<Integer, Integer> map = new HashMap<>();
        for (int i = 0; i < array.length; i++) {
            int temp = amount - array[i];
            if (map.containsKey(temp)) {
                System.out.println("" + array[i] + ":" + temp);
            }
            map.put(array[i], i);
        }
    }

}