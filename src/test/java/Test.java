import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

class Test {

    public static void main(String[] args) {
        MyThread[] s = new MyThread[8];
        for (int i = 0; i < 8; i++) {
            s[i] = new MyThread();
        }
        System.out.println(Arrays.toString(s));
        MyThread t1 = new MyThread();
        MyThread t2 = new MyThread();
        t2.start();
        t1.start();
//        MyThread1 t0 = new MyThread1();
//        Thread t3 = new Thread(t0, "t3");
//        Thread t4 = new Thread(t0, "t4");
//        t3.start();
//        t4.start();
    }


    static class MyThread extends Thread {
        private int ticket = 5;
        private final static Object lock = new Object();

        public void run() {
            synchronized (lock) {
                while (ticket > 0) {
                    System.out.println(Thread.currentThread().getName() + " " + "Thread ticket = " + ticket--);
                }
            }
        }

    }

    static class MyThread1 implements Runnable {
        private static int ticket = 10;

        public void run() {
            synchronized (this) {
                while (ticket > 0) {
                    System.out.println(Thread.currentThread().getName() + " " + "Runnable ticket = " + ticket--);
                }
            }
        }
    }

}
