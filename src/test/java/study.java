import org.testng.annotations.Test;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;


public class study {

    //    private int[] numbers = new int[]{40, 54, 64, 14, 32, 1, 4, 9, 10, 7, 7, 8, 10, 15, 67, 87, 23, 45};
    private int[] numbers = new int[]{20, 20, 6, 1, 3, 9, 34, 27, 18, 28, 28, 87, 73, 90};

    @Test
    public void run() {
        quickSort(numbers, 0, numbers.length - 1);
        System.out.println(Arrays.toString(numbers));
    }

    private void quickSort(int[] array, int low, int high) {
        int i, j, temp, t;
        if (low >= high) {
            return;
        }
        i = low;
        j = high;
        temp = array[i];
        while (i < j) {
            while (temp <= array[j] && i < j) {
                j--;
            }
            while (temp >= array[i] && i < j) {
                i++;
            }
            if (i < j) {
                t = array[j];
                array[j] = array[i];
                array[i] = t;
            }
        }
        array[low] = array[i];
        array[i] = temp;
        quickSort(array, low, j - 1);
        quickSort(array, j + 1, high);
    }

    @Test
    public void bubbleSort() {

        int len = numbers.length;
        int temp;
        for (int i = 0; i < len - 1; i++) {
            for (int j = 0; j < len - i - 1; j++) {
                if (numbers[j] > numbers[j + 1]) {
                    temp = numbers[j];
                    numbers[j] = numbers[j + 1];
                    numbers[j + 1] = temp;
                }
            }
        }
        System.out.println(Arrays.toString(numbers));
    }

    public static int lengthOfLongestSubstring(String s) {
        int length = s.length();
        int max = 0;

        Map<Character, Integer> map = new HashMap<>();
        for(int start = 0, end = 0; end < length; end++){
            char element = s.charAt(end);
            if(map.containsKey(element)){
                start = Math.max(map.get(element) + 1, start); //map.get()的地方进行+1操作
            }
            max = Math.max(max, end - start + 1);
            map.put(element, end);
        }
        return max;
    }


    public static int max(String s) {
//        saacdokka
        int length = s.length();
        int max = 0;
        Map<Character, Integer> map = new HashMap<>();
        for (int start = 0, end = 0; end < length; end++) {
            char element = s.charAt(end);
            if (map.containsKey(element)) {
                start = Math.max(map.get(element) + 1, start);
            }
            max = Math.max(max, end - start + 1);
            map.put(element, end);
        }
        return max;
    }

    public static void merge(int[] num1, int m, int[] num2, int n) {
        int len1 = m - 1;
        int len2 = n - 1;
        int len = m + n - 1;
        while (len1 >= 0 && len2 >= 0) {
            num1[len--] = num1[len1] > num2[len2] ? num1[len1--] : num2[len2--];
        }
        System.arraycopy(num2, 0, num1, 0, len2 + 1);
        System.out.println(Arrays.toString(num1));
    }

    public static int removeDuplicates(int[] nums) {
        if (nums == null || nums.length == 0) {
            return 0;
        }
        int i = 0;
        int j = 1;
        while (j < nums.length) {
            if (nums[i] != nums[j]) {
                if (j - i > 1) {
                    nums[i + 1] = nums[j];
                }
                i++;
            }
            j++;
        }
        System.out.println(Arrays.toString(nums));
        return i + 1;
    }

    public static void main(String[] args) {
        String s = "cbaebabacd";
        int max = lengthOfLongestSubstring(s);
        System.out.println(max);
//        int[] num1 = new int[]{1, 3, 5, 5, 7};
//        System.out.println(removeDuplicates(num1));
    }

}

