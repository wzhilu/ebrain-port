import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

public class Reflection {

    public static void main(String[] args) throws Exception {

        System.out.println(getClientDate(3));
    }

    public static String getClientDate(long addition) {
        DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy-MM-dd");
        return dtf.format(LocalDate.now().plusDays(addition));
    }


}