package com.ebrainPort.job;

import com.ebrainPort.trigger.AutoExport;
import org.quartz.*;

public class StrategyJob implements Job {

    @Override
    public void execute(JobExecutionContext jobExecutionContext) {
        try {
            System.out.println(jobExecutionContext.getFireTime());
            System.out.println(jobExecutionContext.getPreviousFireTime());
            System.out.println(jobExecutionContext.getNextFireTime());
            System.out.println(jobExecutionContext.getMergedJobDataMap());
            AutoExport.run();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }


}