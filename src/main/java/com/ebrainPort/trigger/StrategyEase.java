package com.ebrainPort.trigger;

import com.ebrainPort.domain.Config;
import com.ebrainPort.domain.InitConfig;
import com.ebrainPort.utils.MyUtil;
import com.ebrainPort.utils.SshLinux;

import ch.ethz.ssh2.Connection;

public class StrategyEase {

    public static void main(String[] args) throws Exception {
//        InitConfig.init();
        obtainLog();
    }

    public static String[] obtainLog() throws Exception {
        String date = MyUtil.getNowDate();
        Connection conn = SshLinux.openConnection(Config.getVnpy_ip(), Config.getVnpy_port(), Config.getVnpy_username(), Config.getVnpy_password());
        StringBuilder cmd = new StringBuilder("egrep \"全部成交\" .vntrader/log/knife/qt_")
                .append(date).append(".log .vntrader/log/price_diff/qt_")
                .append(date).append(".log .vntrader/log/break/qt_")
                .append(date).append(".log").append(" ")
                .append("| awk -F.log: '{print $2}'");
        System.out.println(cmd.toString());
        String result = SshLinux.execmd(conn, cmd.toString());
        assert result != null;
        String[] resultArray = result.split("\r?\n");
        for (String order : resultArray) {
            System.out.println(order);
        }
//        int resultCount = resultArray.length;
//        System.out.println(resultCount);
//        List<List<String>> totalList = new ArrayList<>();
//        for (String trade : resultArray) {
//            List<String> tradeList = MyUtil.myMatcher(trade, "(\\d*-\\d*-\\d*\\s\\d*:\\d*:\\d*,\\d*).*(\\{.*})");
//            JSONObject tradeProject = new JSONObject(tradeList.get(1));
//            String strategy = (String) tradeProject.get("strategy_name");
//            String timestamp = tradeProject.get("timestamp").toString();
//            List<String> strategyList = MyUtil.myMatcher(strategy, "(\\d{6})\\_(.*)");
//            List<String> casualList = List.of(timestamp, tradeList.get(0), strategyList.get(0), strategyList.get(1), tradeList.get(1));
//            totalList.add(casualList);
//        }
//        totalList.sort(Comparator.comparing((List list) -> list.get(0).toString()));
//        totalList.forEach(System.out::println);
        return resultArray;
    }

}


