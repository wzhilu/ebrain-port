package com.ebrainPort.trigger;

import com.ebrainPort.domain.Config;
import com.ebrainPort.utils.MyUtil;
import com.ebrainPort.utils.HttpClientUtils;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.time.Duration;
import java.time.LocalDateTime;
import java.util.*;

import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.apache.kafka.clients.consumer.ConsumerRecords;
import org.apache.kafka.clients.consumer.KafkaConsumer;
import org.json.JSONObject;
import org.testng.annotations.Test;

public class ReadKafka {

    private static final String RESULT_PATH = MyUtil.obtainOutputPath();

    public static void main(String[] args) throws IOException {
        Properties props = new Properties();
        props.put("bootstrap.servers", Config.getKafka_ip() + ":" + Config.getKafka_port());
        props.put("group.id", Config.getKafka_groupId());
        props.put("enable.auto.commit", "true");
        //想要读取之前的数据，必须加上
        //props.put("auto.offset.reset", "earliest");
        /* 自动确认offset的时间间隔 */
        props.put("auto.commit.interval.ms", "1000");
        /*
         * 一旦consumer和kafka集群建立连接，
         * consumer会以心跳的方式来高速集群自己还活着，
         * 如果session.timeout.ms 内心跳未到达服务器，服务器认为心跳丢失，会做rebalence
         */
        props.put("session.timeout.ms", "30000");
        props.put("key.deserializer", "org.apache.kafka.common.serialization.StringDeserializer");
        props.put("value.deserializer", "org.apache.kafka.common.serialization.StringDeserializer");
        //配置自定义的拦截器，可以在拦截器中引入第三方插件实现日志记录等功能。
        //props.put("interceptor.classes", "com.lt.kafka.consumer.MyConsumerInterceptor");

        KafkaConsumer<String, String> consumer = new KafkaConsumer<>(props);
        File file = new File(RESULT_PATH + "failed.txt");
        BufferedWriter writeTXT = new BufferedWriter(new FileWriter(file));
        try {
            /* 消费者订阅的topic, 可同时订阅多个 ，用逗号隔开*/
            String[] topicArray = Config.getKafka_topic();
            consumer.subscribe(Arrays.asList(topicArray[0], topicArray[1], topicArray[2]));
            while (true) {
                //轮询数据。如果缓冲区中没有数据，轮询等待的时间为毫秒。如果0，立即返回缓冲区中可用的任何记录，否则返回空
                ConsumerRecords<String, String> records = consumer.poll(Duration.ofMillis(1000));
                long start = System.currentTimeMillis();
                for (ConsumerRecord<String, String> record : records) {
                    JSONObject recordObject = new JSONObject(record.value());
                    String message = recordObject.get("message").toString();
                    if (message.contains("on_order")) {
                        System.out.println(message);
                        Thread t = new MyThread(message);
                        t.start();
                    } else if (message.contains("order failed")) {
                        System.out.println(message);
                        sendMessage(1, message);
                        writeTXT.write(message);
                        writeTXT.newLine();
                    }
                }
                long end = System.currentTimeMillis();
                System.out.println(records.count());
                System.out.println(LocalDateTime.now() + " " + "used time:" + (end - start) + "ms");
                writeTXT.flush();
            }
        } catch (Exception e) {
            // TODO: handle exception
            e.printStackTrace();
            writeTXT.close();
        }
    }

    static class MyThread extends Thread {

        private MyThread(String order) {
            this.order = order;
        }

        private String order;

        @Override
        public void run() {
            insertByOrder(order);
        }
    }

    private static void insertByOrder(String order) {
        if (order.contains("全部成交")) {
            List<String> orderList = MyUtil.myMatcher(order, "(\\d*-\\d*-\\d*)\\s(\\d*:\\d*:\\d*,\\d*).*(\\{.*})");
            JSONObject orderProject = new JSONObject(orderList.get(2));
            String strategy = orderProject.get("n").toString();
            if (!strategy.isEmpty()) {
                sendMessage(2, order);
                List<String> strategyList = MyUtil.myMatcher(strategy, "(\\d{6})\\_(.*)");
                JSONObject jsonObject = new JSONObject();
                jsonObject.put("trade_date", orderList.get(0));
                jsonObject.put("order_time", orderList.get(1));
                jsonObject.put("point", orderProject.get("point").toString());
                jsonObject.put("stock_code", strategyList.get(0));
                jsonObject.put("strategy", strategyList.get(1));
                jsonObject.put("l_oid", orderProject.get("l_oid").toString());
                jsonObject.put("contract_number", orderProject.get("s_oid").toString());
                jsonObject.put("order_type", orderProject.get("d").toString());
                jsonObject.put("order_action", orderProject.get("o").toString());
                jsonObject.put("order_price", orderProject.getDouble("p"));
                jsonObject.put("stock_amount", orderProject.getInt("v"));
                jsonObject.put("order_status", orderProject.get("status").toString());
                jsonObject.put("ts", orderProject.get("ts").toString());
                HttpClientUtils.postJsonToString(Config.getUrl_analysis() + "insertVnpyOrder", null, jsonObject.toString());
            }
        }
    }

    @Test
    private static void manualSupplementRecord() throws Exception {
        String[] records = StrategyEase.obtainLog();
        for (String record : records) {
            insertByOrder(record);
        }
    }

    @Test
    private static void sendMessage(int kind, String text) {
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("type", kind);
        jsonObject.put("message", text);
        HttpClientUtils.postJsonToString(Config.getUrl_inform(), null, jsonObject.toString());
    }

}

