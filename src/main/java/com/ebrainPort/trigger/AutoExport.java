package com.ebrainPort.trigger;

import com.ebrainPort.domain.Config;
import com.ebrainPort.utils.MyUtil;
import com.ebrainPort.utils.HttpClientUtils;
import com.ebrainPort.utils.SendEmailUtil;

import java.io.*;
import java.time.LocalDate;
import java.util.*;

import org.json.JSONArray;
import org.json.JSONObject;
import org.testng.annotations.Test;

public class AutoExport {

    private static final String BUY = "买入";
    private static final String RESULT_PATH = MyUtil.obtainOutputPath();
    private static String today;
    private static int state;
    private static String targetTime;
    private static Map<String, String> mapUrl;

    static {
        mapUrl = new HashMap<>();
        mapUrl.put("status", "FILLED");
    }

    public static void run() throws InterruptedException {
        while (true) {
            targetTime = MyUtil.getNowTime();
            System.out.println(targetTime);
            if (targetTime.equals(Config.getTime_test())) {
                Thread t = new MyThread();
                t.start();
            } else if (targetTime.equals(Config.getTime_reality())) {
                Thread t1 = new MyThread();
                t1.start();
            }
            System.out.println("Delay 60s");
            Thread.sleep(59999);
        }
    }

    static class MyThread extends Thread {
        @Override
        public void run() {
            today = LocalDate.now().toString();
            insertByDeal();
            insertByDelegation();
            insertByProfit();
            sendEmail();
            state = 0;
        }
    }

    @Test
    private static void sendEmail() {
        String headline = today + "模拟盘日报";
        String html = assembleHtmlByProfit();
        String profitTotal = HttpClientUtils.getString(Config.getUrl_analysis() + "profit/total/" + today, null, null);
        assert profitTotal != null;
        if (profitTotal.isEmpty()) {
            profitTotal = "0";
        }
//        state = 0;
        String to = "";
        String cc = "";
        String subject = "";
        String content = "";
        String logFile = "";
        switch (state) {
            case 0:
                to = "wangyugang@ebrain.ai";
                cc = "wangyugang@ebrain.ai";
                subject = headline;
                content = "概要：今日盈利总额" + profitTotal + "元。\n" + html;
                logFile = "nothing";
                break;
            case 1:
                to = "13816224466@qq.com,1768057784@qq.com";
                cc = "xiafen@ebrain.ai,migto@163.com,zhongchongguang@ebrain.ai,liuchao@ebrain.ai,wangyugang@ebrain.ai";
                subject = headline;
                content = "概要：今日盈利总额" + profitTotal + "元。\n" + html;
                logFile = "nothing";
                break;
            case 2:
                to = "13816224466@qq.com,1768057784@qq.com";
                cc = "xiafen@ebrain.ai,migto@163.com,zhongchongguang@ebrain.ai,liuchao@ebrain.ai,wangyugang@ebrain.ai";
                subject = today + "没有成功交易记录。";
                content = "概要：今日盈利总额" + profitTotal + "元。\n" + html;
                logFile = "nothing";
                break;
            default:
                System.out.println("No need to execute");
        }
        SendEmailUtil sendEmailUtil = new SendEmailUtil(to, cc, subject, content, logFile);
        sendEmailUtil.send();
    }

    private static String assembleHtmlByProfit() {
        String profits = HttpClientUtils.getString(Config.getUrl_analysis() + "profit/" + today, null, null);
        assert profits != null;
        if (!profits.equals("[]") && !targetTime.equals(Config.getTime_test())) {
            state = 1;
        } else if (profits.equals("[]") && !targetTime.equals(Config.getTime_test())) {
            state = 2;
        }
        System.out.println(state);
        JSONArray profitsObject = new JSONArray(profits);
        String htmlPrefix = "<div><table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" width=\"815\" style=\"border-collapse:collapse;width:610pt\"><colgroup><col width=\"178\" style=\"mso-width-source:userset;mso-width-alt:6343;width:134pt\"><col width=\"91\" span=\"7\" style=\"mso-width-source:userset;mso-width-alt:3242;width:68pt\"></colgroup><tbody><tr height=\"23\" style=\"height:17.4pt\"><td height=\"23\" class=\"xl65\" width=\"170\" style=\"height:17.4pt;width:108pt\">策略名称</td><td class=\"xl65\" width=\"91\" style=\"width:68pt\">证券代码</td><td class=\"xl65\" width=\"91\" style=\"width:68pt\">证券名称</td><td class=\"xl65\" width=\"91\" style=\"width:68pt\">买入时间</td><td class=\"xl65\" width=\"91\" style=\"width:68pt\">卖出时间</td><td class=\"xl65\" width=\"91\" style=\"width:68pt\">买入金额</td><td class=\"xl65\" width=\"91\" style=\"width:68pt\">卖出金额</td><td class=\"xl65\" width=\"91\" style=\"width:68pt\">盈利金额</td>";
        String htmlPostfix = "</tr></tbody></table></div><div><includetail><!--<![endif]--></includetail></div>";
        StringBuilder stringBuilder = new StringBuilder();
        for (Object profit : profitsObject) {
            JSONObject jsonObject = (JSONObject) profit;
            String buy_time = jsonObject.get("buy_time").toString();
            if (buy_time.equals("null")) {
                buy_time = "无买入";
            }
            String sell_time = jsonObject.get("sell_time").toString();
            if (sell_time.equals("null")) {
                sell_time = "无卖出";
            }
            stringBuilder.append("</tr><tr height=\"19\" style=\"height:14.4pt\"><td height=\"19\" class=\"xl66\" style=\"height:14.4pt\">")
                    .append(jsonObject.get("strategy")).append("</td>")
                    .append("<td class=\"xl66\">").append(jsonObject.get("stock_code")).append("</td>")
                    .append("<td class=\"xl66\">").append(jsonObject.get("stock_name")).append("</td>")
                    .append("<td class=\"xl66\">").append(buy_time).append("</td>")
                    .append("<td class=\"xl66\">").append(sell_time).append("</td>")
                    .append("<td class=\"xl66\">").append(jsonObject.get("buy_sum")).append("</td>")
                    .append("<td class=\"xl66\">").append(jsonObject.get("sell_sum")).append("</td>")
                    .append("<td class=\"xl66\">").append(jsonObject.get("profit_sum")).append("</td>");
        }
        return htmlPrefix + stringBuilder + htmlPostfix;
    }

    @Test
    public static void insertByProfit() {
        String income = HttpClientUtils.getString(Config.getUrl_analysis() + "deal/" + today, null, null);
        assert income != null;
        JSONArray incomeObject = new JSONArray(income);
        int length = incomeObject.length();
        Set<Integer> set = new HashSet<>();
        set.add(-1);
        for (int i = 0; i < length; i++) {
            JSONObject outStock = (JSONObject) incomeObject.get(i);
            JSONObject jsonObject = new JSONObject();
            if (!set.contains(i)) {
                jsonObject.put("trade_date", outStock.get("trade_date"));
                jsonObject.put("strategy", outStock.get("strategy"));
                jsonObject.put("stock_code", outStock.get("stock_code"));
                jsonObject.put("stock_name", outStock.get("stock_name"));
                if (outStock.get("operation").equals(BUY)) {
                    jsonObject.put("buy_time", outStock.get("deal_time"));
                    jsonObject.put("buy_sum", outStock.get("deal_sum"));
                    jsonObject.put("buy_contract_number", outStock.get("contract_number"));
                } else {
                    jsonObject.put("sell_time", outStock.get("deal_time"));
                    jsonObject.put("sell_sum", outStock.get("deal_sum"));
                    jsonObject.put("sell_contract_number", outStock.get("contract_number"));
                }
                for (int j = i + 1; j < length; j++) {
                    JSONObject inStock = (JSONObject) incomeObject.get(j);
                    if (outStock.get("strategy").equals(inStock.get("strategy")) && outStock.get("stock_code").equals(inStock.get("stock_code")) && !outStock.get("operation").equals(inStock.get("operation")) && !set.contains(j)) {
                        double preSum = outStock.getDouble("deal_sum");
                        double nextSum = inStock.getDouble("deal_sum");
                        double profitSum;
                        if (inStock.get("operation").equals(BUY)) {
                            profitSum = preSum - nextSum;
                            jsonObject.put("buy_time", inStock.get("deal_time"));
                            jsonObject.put("buy_sum", inStock.get("deal_sum"));
                            jsonObject.put("buy_contract_number", inStock.get("contract_number"));
                        } else {
                            profitSum = nextSum - preSum;
                            jsonObject.put("sell_time", inStock.get("deal_time"));
                            jsonObject.put("sell_sum", inStock.get("deal_sum"));
                            jsonObject.put("sell_contract_number", inStock.get("contract_number"));
                        }
                        jsonObject.put("profit_sum", profitSum);
                        set.add(j);
                        break;
                    }
                }
            }
            if (!jsonObject.isEmpty()) {
                HttpClientUtils.postJsonToString(Config.getUrl_analysis() + "insertProfit", null, jsonObject.toString());
            }
        }
    }

    @Test
    public static void insertByDeal() {
        JSONArray rows = getTraderData(Config.getUrl_strategy() + "orders", mapUrl);
        if (!rows.isEmpty()) {
            for (Object deal : rows) {
                JSONObject jsonObject = new JSONObject();
                JSONArray perDeal = (JSONArray) deal;
                jsonObject.put("trade_date", today);
                jsonObject.put("deal_time", perDeal.get(0));
                jsonObject.put("stock_code", perDeal.get(1));
                jsonObject.put("stock_name", perDeal.get(2));
                jsonObject.put("operation", perDeal.get(3));
                jsonObject.put("deal_amount", perDeal.getInt(4));
                jsonObject.put("deal_average_price", perDeal.get(5));
                jsonObject.put("deal_sum", perDeal.getDouble(6));
                jsonObject.put("contract_number", perDeal.get(7));
                jsonObject.put("deal_number", perDeal.get(8));
                HttpClientUtils.postJsonToString(Config.getUrl_analysis() + "insertDeal", null, jsonObject.toString());
            }
        }
    }

    @Test
    public static void insertByDelegation() {
        JSONArray rows = getTraderData(Config.getUrl_strategy() + "orders", null);
        if (!rows.isEmpty()) {
            for (Object delegation : rows) {
                JSONObject jsonObject = new JSONObject();
                JSONArray perDelegation = (JSONArray) delegation;
                jsonObject.put("trade_date", today);
                jsonObject.put("delegation_time", perDelegation.get(0));
                jsonObject.put("stock_code", perDelegation.get(1));
                jsonObject.put("stock_name", perDelegation.get(2));
                jsonObject.put("operation", perDelegation.get(3));
                jsonObject.put("remark", perDelegation.get(4));
                jsonObject.put("delegation_amount", perDelegation.get(5));
                jsonObject.put("deal_amount", perDelegation.get(6));
                jsonObject.put("delegation_price", perDelegation.get(7));
                jsonObject.put("deal_average_price", perDelegation.get(8));
                jsonObject.put("repeal_amount", perDelegation.get(9));
                jsonObject.put("contract_number", perDelegation.get(10));
                jsonObject.put("trade_market", perDelegation.get(11));
                jsonObject.put("shareholder_account", perDelegation.get(12));
                System.out.println(jsonObject.toString());
                HttpClientUtils.postJsonToString(Config.getUrl_analysis() + "insertDelegation", null, jsonObject.toString());
            }
        }
    }

    private static JSONArray getTraderData(String url, Map<String, String> params) {
        String response;
        if (params != null && !params.isEmpty()) {
            response = HttpClientUtils.getString(url, params, null);
//            response = "{\"count\":5,\"dataTable\":{\"columns\":[\"成交时间\",\"证券代码\",\"证券名称\",\"操作\",\"成交数量\",\"成交均价\",\"成交金额\",\"合同编号\",\"成交编号\"],\"rows\":[[\"09:30:03\",\"603956\",\"威派格\",\"买入\",\"100\",\"16.790\",\"1679.000\",\"1318911659\",\"2922362114\"]]},\"startDate\":\"1970-01-01T08:00:00\",\"endDate\":\"1970-01-01T08:00:00\",\"updateDate\":\"2019-12-31T19:23:38\"}";

        } else {
            response = HttpClientUtils.getString(url, null, null);
//            response = "{\"count\":66,\"dataTable\":{\"columns\":[\"委托时间\",\"证券代码\",\"证券名称\",\"操作\",\"备注\",\"委托数量\",\"成交数量\",\"委托价格\",\"成交均价\",\"撤消数量\",\"合同编号\",\"交易市场\",\"股东帐户\"],\"rows\":[[\"15:44:16\",\"603496\",\"恒为科技\",\"买入\",\"全部撤单\",\"100\",\"0\",\"22.300\",\"0.000\",\"100\",\"1317027979\",\"上海Ａ股\",\"A484614045\"],[\"15:44:17\",\"603722\",\"阿科力\",\"买入\",\"全部撤单\",\"100\",\"0\",\"31.750\",\"0.000\",\"100\",\"1317006824\",\"上海Ａ股\",\"A484614045\"],[\"15:44:19\",\"603956\",\"威派格\",\"买入\",\"全部撤单\",\"100\",\"0\",\"16.900\",\"0.000\",\"100\",\"1317021170\",\"上海Ａ股\",\"A484614045\"],[\"15:44:28\",\"603327\",\"福蓉科技\",\"买入\",\"全部撤单\",\"100\",\"0\",\"29.000\",\"0.000\",\"100\",\"1317023179\",\"上海Ａ股\",\"A484614045\"],[\"15:46:58\",\"603626\",\"科森科技\",\"买入\",\"全部撤单\",\"100\",\"0\",\"10.610\",\"0.000\",\"100\",\"1317013089\",\"上海Ａ股\",\"A484614045\"],[\"15:52:31\",\"603722\",\"阿科力\",\"买入\",\"全部撤单\",\"100\",\"0\",\"31.930\",\"0.000\",\"100\",\"1317007323\",\"上海Ａ股\",\"A484614045\"],[\"16:27:14\",\"603496\",\"恒为科技\",\"买入\",\"全部撤单\",\"100\",\"0\",\"22.300\",\"0.000\",\"100\",\"1317023331\",\"上海Ａ股\",\"A484614045\"],[\"16:27:15\",\"603722\",\"阿科力\",\"买入\",\"全部撤单\",\"100\",\"0\",\"31.750\",\"0.000\",\"100\",\"1317012042\",\"上海Ａ股\",\"A484614045\"],[\"16:27:17\",\"603956\",\"威派格\",\"买入\",\"全部撤单\",\"100\",\"0\",\"16.900\",\"0.000\",\"100\",\"1317006276\",\"上海Ａ股\",\"A484614045\"],[\"16:27:43\",\"603327\",\"福蓉科技\",\"买入\",\"全部撤单\",\"100\",\"0\",\"29.000\",\"0.000\",\"100\",\"1317007082\",\"上海Ａ股\",\"A484614045\"],[\"16:34:14\",\"603626\",\"科森科技\",\"买入\",\"全部撤单\",\"100\",\"0\",\"10.610\",\"0.000\",\"100\",\"1317014527\",\"上海Ａ股\",\"A484614045\"],[\"17:43:27\",\"000823\",\"超声电子\",\"买入\",\"全部撤单\",\"100\",\"0\",\"15.050\",\"0.000\",\"100\",\"1317184555\",\"深圳Ａ股\",\"00108893711\"],[\"17:43:51\",\"000823\",\"超声电子\",\"买入\",\"全部撤单\",\"100\",\"0\",\"15.080\",\"0.000\",\"100\",\"1317028450\",\"深圳Ａ股\",\"00108893711\"],[\"17:44:19\",\"002442\",\"龙星化工\",\"买入\",\"全部撤单\",\"100\",\"0\",\"6.540\",\"0.000\",\"100\",\"1317006841\",\"深圳Ａ股\",\"00108893711\"],[\"17:44:37\",\"002442\",\"龙星化工\",\"买入\",\"全部撤单\",\"100\",\"0\",\"6.650\",\"0.000\",\"100\",\"1317023092\",\"深圳Ａ股\",\"00108893711\"],[\"17:44:58\",\"002442\",\"龙星化工\",\"买入\",\"全部撤单\",\"100\",\"0\",\"6.670\",\"0.000\",\"100\",\"1317023385\",\"深圳Ａ股\",\"00108893711\"],[\"17:46:33\",\"300585\",\"奥联电子\",\"买入\",\"全部撤单\",\"100\",\"0\",\"17.100\",\"0.000\",\"100\",\"1317007337\",\"深圳Ａ股\",\"00108893711\"],[\"17:46:48\",\"300585\",\"奥联电子\",\"买入\",\"全部撤单\",\"100\",\"0\",\"17.160\",\"0.000\",\"100\",\"1317006292\",\"深圳Ａ股\",\"00108893711\"],[\"17:46:54\",\"300585\",\"奥联电子\",\"买入\",\"全部撤单\",\"100\",\"0\",\"17.180\",\"0.000\",\"100\",\"1317019551\",\"深圳Ａ股\",\"00108893711\"],[\"17:47:25\",\"300460\",\"惠伦晶体\",\"买入\",\"全部撤单\",\"100\",\"0\",\"17.700\",\"0.000\",\"100\",\"1317023036\",\"深圳Ａ股\",\"00108893711\"],[\"17:48:10\",\"300787\",\"海能实业\",\"买入\",\"全部撤单\",\"100\",\"0\",\"86.880\",\"0.000\",\"100\",\"1317023093\",\"深圳Ａ股\",\"00108893711\"],[\"17:48:28\",\"300787\",\"海能实业\",\"买入\",\"全部撤单\",\"100\",\"0\",\"86.620\",\"0.000\",\"100\",\"1317184556\",\"深圳Ａ股\",\"00108893711\"],[\"17:48:41\",\"300460\",\"惠伦晶体\",\"买入\",\"全部撤单\",\"100\",\"0\",\"17.700\",\"0.000\",\"100\",\"1317023037\",\"深圳Ａ股\",\"00108893711\"],[\"17:48:54\",\"300787\",\"海能实业\",\"买入\",\"全部撤单\",\"100\",\"0\",\"85.500\",\"0.000\",\"100\",\"1317019141\",\"深圳Ａ股\",\"00108893711\"],[\"17:48:59\",\"300460\",\"惠伦晶体\",\"买入\",\"全部撤单\",\"100\",\"0\",\"17.790\",\"0.000\",\"100\",\"1317023193\",\"深圳Ａ股\",\"00108893711\"],[\"17:50:24\",\"000835\",\"长城动漫\",\"买入\",\"全部撤单\",\"100\",\"0\",\"5.360\",\"0.000\",\"100\",\"1317019593\",\"深圳Ａ股\",\"00108893711\"],[\"17:50:42\",\"000835\",\"长城动漫\",\"买入\",\"全部撤单\",\"100\",\"0\",\"5.370\",\"0.000\",\"100\",\"1317005948\",\"深圳Ａ股\",\"00108893711\"],[\"17:52:50\",\"600695\",\"绿庭投资\",\"买入\",\"全部撤单\",\"100\",\"0\",\"10.010\",\"0.000\",\"100\",\"1317023349\",\"上海Ａ股\",\"A484614045\"],[\"17:53:05\",\"600695\",\"绿庭投资\",\"买入\",\"全部撤单\",\"100\",\"0\",\"10.060\",\"0.000\",\"100\",\"1317014348\",\"上海Ａ股\",\"A484614045\"],[\"17:53:21\",\"002442\",\"龙星化工\",\"买入\",\"全部撤单\",\"100\",\"0\",\"6.640\",\"0.000\",\"100\",\"1317012590\",\"深圳Ａ股\",\"00108893711\"],[\"17:53:36\",\"002442\",\"龙星化工\",\"买入\",\"全部撤单\",\"100\",\"0\",\"6.680\",\"0.000\",\"100\",\"1317028451\",\"深圳Ａ股\",\"00108893711\"],[\"17:53:48\",\"002442\",\"龙星化工\",\"买入\",\"全部撤单\",\"100\",\"0\",\"6.700\",\"0.000\",\"100\",\"1317023350\",\"深圳Ａ股\",\"00108893711\"],[\"17:54:00\",\"002442\",\"龙星化工\",\"买入\",\"全部撤单\",\"100\",\"0\",\"6.700\",\"0.000\",\"100\",\"1317023194\",\"深圳Ａ股\",\"00108893711\"],[\"09:30:02\",\"603956\",\"威派格\",\"买入\",\"全部成交\",\"100\",\"100\",\"16.550\",\"16.500\",\"0\",\"1317562056\",\"上海Ａ股\",\"A484614045\"],[\"09:30:08\",\"603956\",\"威派格\",\"卖出\",\"全部成交\",\"100\",\"100\",\"16.530\",\"16.550\",\"0\",\"1317558759\",\"上海Ａ股\",\"A484614045\"],[\"09:30:10\",\"300749\",\"顶固集创\",\"买入\",\"全部成交\",\"100\",\"100\",\"12.670\",\"12.650\",\"0\",\"1317562058\",\"深圳Ａ股\",\"00108893711\"],[\"09:30:20\",\"300749\",\"顶固集创\",\"卖出\",\"全部撤单\",\"100\",\"0\",\"12.860\",\"0.000\",\"100\",\"1317559412\",\"深圳Ａ股\",\"00108893711\"],[\"09:30:47\",\"300749\",\"顶固集创\",\"卖出\",\"全部成交\",\"100\",\"100\",\"12.660\",\"12.680\",\"0\",\"1317542523\",\"深圳Ａ股\",\"00108893711\"],[\"09:30:50\",\"300749\",\"顶固集创\",\"卖出\",\"全部成交\",\"100\",\"100\",\"12.660\",\"12.680\",\"0\",\"1317530821\",\"深圳Ａ股\",\"00108893711\"],[\"09:31:11\",\"300694\",\"蠡湖股份\",\"买入\",\"全部撤单\",\"100\",\"0\",\"14.540\",\"0.000\",\"100\",\"1317550516\",\"深圳Ａ股\",\"00108893711\"],[\"09:31:12\",\"300749\",\"顶固集创\",\"买入\",\"全部成交\",\"100\",\"100\",\"12.710\",\"12.690\",\"0\",\"1317550914\",\"深圳Ａ股\",\"00108893711\"],[\"09:31:35\",\"603681\",\"永冠新材\",\"买入\",\"全部成交\",\"100\",\"100\",\"25.210\",\"25.190\",\"0\",\"1317546169\",\"上海Ａ股\",\"A484614045\"],[\"09:31:44\",\"603681\",\"永冠新材\",\"卖出\",\"全部成交\",\"100\",\"100\",\"25.480\",\"25.490\",\"0\",\"1317536120\",\"上海Ａ股\",\"A484614045\"],[\"09:32:17\",\"603681\",\"永冠新材\",\"卖出\",\"全部成交\",\"100\",\"100\",\"25.050\",\"25.130\",\"0\",\"1317544472\",\"上海Ａ股\",\"A484614045\"],[\"09:32:47\",\"603681\",\"永冠新材\",\"买入\",\"全部成交\",\"100\",\"100\",\"25.250\",\"25.130\",\"0\",\"1317542374\",\"上海Ａ股\",\"A484614045\"],[\"09:33:03\",\"603956\",\"威派格\",\"买入\",\"全部撤单\",\"100\",\"0\",\"16.060\",\"0.000\",\"100\",\"1317558617\",\"上海Ａ股\",\"A484614045\"],[\"09:33:14\",\"603681\",\"永冠新材\",\"卖出\",\"全部成交\",\"100\",\"100\",\"25.160\",\"25.160\",\"0\",\"1317543066\",\"上海Ａ股\",\"A484614045\"],[\"09:33:28\",\"002881\",\"美格智能\",\"买入\",\"全部成交\",\"100\",\"100\",\"26.480\",\"26.440\",\"0\",\"1317542323\",\"深圳Ａ股\",\"00108893711\"],[\"09:33:36\",\"603681\",\"永冠新材\",\"买入\",\"全部撤单\",\"100\",\"0\",\"25.320\",\"0.000\",\"100\",\"1317566059\",\"上海Ａ股\",\"A484614045\"],[\"09:33:42\",\"603681\",\"永冠新材\",\"买入\",\"全部成交\",\"100\",\"100\",\"25.320\",\"25.260\",\"0\",\"1317561567\",\"上海Ａ股\",\"A484614045\"],[\"09:33:51\",\"603681\",\"永冠新材\",\"买入\",\"全部成交\",\"100\",\"100\",\"25.260\",\"25.250\",\"0\",\"1317550922\",\"上海Ａ股\",\"A484614045\"],[\"09:34:43\",\"002881\",\"美格智能\",\"卖出\",\"全部成交\",\"100\",\"100\",\"26.780\",\"26.780\",\"0\",\"1317559823\",\"深圳Ａ股\",\"00108893711\"],[\"09:37:06\",\"300694\",\"蠡湖股份\",\"买入\",\"全部成交\",\"100\",\"100\",\"14.020\",\"14.000\",\"0\",\"1317559273\",\"深圳Ａ股\",\"00108893711\"],[\"09:37:12\",\"300694\",\"蠡湖股份\",\"卖出\",\"全部成交\",\"100\",\"100\",\"14.110\",\"14.140\",\"0\",\"1317552621\",\"深圳Ａ股\",\"00108893711\"],[\"09:37:32\",\"300694\",\"蠡湖股份\",\"卖出\",\"全部成交\",\"100\",\"100\",\"13.830\",\"13.850\",\"0\",\"1317552622\",\"深圳Ａ股\",\"00108893711\"],[\"09:37:51\",\"300694\",\"蠡湖股份\",\"买入\",\"全部撤单\",\"100\",\"0\",\"13.540\",\"0.000\",\"100\",\"1317543085\",\"深圳Ａ股\",\"00108893711\"],[\"09:38:27\",\"300694\",\"蠡湖股份\",\"买入\",\"全部成交\",\"100\",\"100\",\"13.690\",\"13.680\",\"0\",\"1317551183\",\"深圳Ａ股\",\"00108893711\"],[\"09:38:48\",\"300694\",\"蠡湖股份\",\"卖出\",\"全部成交\",\"100\",\"100\",\"13.800\",\"13.840\",\"0\",\"1317565281\",\"深圳Ａ股\",\"00108893711\"],[\"09:39:01\",\"300694\",\"蠡湖股份\",\"买入\",\"全部成交\",\"100\",\"100\",\"13.860\",\"13.860\",\"0\",\"1317543889\",\"深圳Ａ股\",\"00108893711\"],[\"09:39:42\",\"300694\",\"蠡湖股份\",\"买入\",\"全部成交\",\"100\",\"100\",\"13.860\",\"13.860\",\"0\",\"1317553776\",\"深圳Ａ股\",\"00108893711\"],[\"09:41:22\",\"300749\",\"顶固集创\",\"买入\",\"全部成交\",\"100\",\"100\",\"12.260\",\"12.260\",\"0\",\"1317537644\",\"深圳Ａ股\",\"00108893711\"],[\"09:41:29\",\"300749\",\"顶固集创\",\"卖出\",\"全部成交\",\"100\",\"100\",\"12.380\",\"12.390\",\"0\",\"1317564036\",\"深圳Ａ股\",\"00108893711\"],[\"11:19:51\",\"300694\",\"蠡湖股份\",\"买入\",\"全部成交\",\"100\",\"100\",\"14.370\",\"14.350\",\"0\",\"1317938705\",\"深圳Ａ股\",\"00108893711\"],[\"11:20:08\",\"300694\",\"蠡湖股份\",\"买入\",\"全部撤单\",\"100\",\"0\",\"14.370\",\"0.000\",\"100\",\"1317940904\",\"深圳Ａ股\",\"00108893711\"],[\"14:29:06\",\"300694\",\"蠡湖股份\",\"买入\",\"全部撤单\",\"100\",\"0\",\"14.400\",\"0.000\",\"100\",\"1318305555\",\"深圳Ａ股\",\"00108893711\"],[\"14:29:53\",\"300694\",\"蠡湖股份\",\"买入\",\"全部成交\",\"100\",\"100\",\"14.530\",\"14.510\",\"0\",\"1318298760\",\"深圳Ａ股\",\"00108893711\"]]},\"startDate\":\"1970-01-01T08:00:00\",\"endDate\":\"1970-01-01T08:00:00\",\"updateDate\":\"2019-12-30T15:54:47\"}";

        }
        assert response != null;
        JSONObject responseObject = new JSONObject(response);
        JSONObject dataTable = (JSONObject) responseObject.get("dataTable");
        return dataTable.getJSONArray("rows");
    }

    @Test
    public static void obtainStockPool() throws IOException {
        JSONArray rows = getTraderData(Config.getUrl_strategy() + "positions", null);
        int length = rows.length();
        File file = new File(RESULT_PATH + "stockPool.txt");
        BufferedWriter writeTXT = new BufferedWriter(new FileWriter(file));
        writeTXT.write("证券代码,证券名称,股票余额,可用余额,冻结数量");
        writeTXT.newLine();
        for (int i = 0; i < length; i++) {
            JSONArray perStock = (JSONArray) rows.get(i);
            String builder = perStock.get(1).toString()
                    + "," + perStock.get(2).toString()
                    + "," + perStock.get(3).toString()
                    + "," + perStock.get(4).toString()
                    + "," + perStock.get(5).toString();
            writeTXT.write(builder);
            writeTXT.newLine();
        }
        writeTXT.flush();
        writeTXT.close();
    }


}