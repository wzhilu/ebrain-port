package com.ebrainPort;

import com.ebrainPort.domain.InitConfig;
import com.ebrainPort.job.StrategyJob;
import org.quartz.*;
import org.quartz.impl.StdSchedulerFactory;


public class MyScheduler {

    public static void main(String[] args) {
         //初始化全局配置
        InitConfig.init();
        //定义一个JobDetail
        JobDetail jobDetail = JobBuilder.newJob(StrategyJob.class)
                //定义name和group
                .withIdentity("job1", "group1")
                //job需要传递的内容
                .usingJobData("name", "AutoExport")
                .build();
        //定义一个Trigger
        Trigger trigger = TriggerBuilder.newTrigger().withIdentity("trigger1", "group1")
                //加入 scheduler之后立刻执行
                .startNow()
                .withSchedule(CronScheduleBuilder.cronSchedule("00 39 16 ? * MON-FRI"))
                .build();
        try {
            //创建scheduler
            System.out.println(System.currentTimeMillis());
            SchedulerFactory schedulerFactory = new StdSchedulerFactory();
            Scheduler scheduler = schedulerFactory.getScheduler();
            scheduler.scheduleJob(jobDetail, trigger);
            System.out.println("--------scheduler start ! ------------");
            scheduler.start();
            Thread.sleep(900000);
            System.out.println("delay 900s");
            System.out.println("--------scheduler shutdown ! ------------");
            scheduler.shutdown();
        } catch (SchedulerException | InterruptedException e) {
            e.printStackTrace();
        }
    }


}