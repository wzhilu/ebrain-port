package com.ebrainPort.domain;

import com.ebrainPort.utils.YmlUtils;
import org.yaml.snakeyaml.DumperOptions;
import org.yaml.snakeyaml.Yaml;
import org.yaml.snakeyaml.error.YAMLException;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.util.LinkedHashMap;

public class InitConfig {

    private final static DumperOptions OPTIONS = new DumperOptions();
    private final static String OUTPATH = System.getProperty("user.dir") + File.separator + "config.yml";
    private static LinkedHashMap ret;

    static {
        //将默认读取的方式设置为块状读取
        OPTIONS.setDefaultFlowStyle(DumperOptions.FlowStyle.BLOCK);
        Yaml yaml = new Yaml();
        try {
            InputStream in = new FileInputStream(new File(OUTPATH));
            ret = yaml.load(in);
        } catch (YAMLException | FileNotFoundException e) {
            ret = yaml.load(YmlUtils.class.getClassLoader().getResourceAsStream("config.yml"));
        }
    }

    public static void init() {

        LinkedHashMap url = (LinkedHashMap) ret.get("url");
        Config.setUrl_analysis(url.get("analysis").toString());
        Config.setUrl_strategy(url.get("strategy").toString());
        Config.setUrl_inform(url.get("inform").toString());

        LinkedHashMap email = (LinkedHashMap) ret.get("email");
        Config.setEmail_account(email.get("account").toString());
        Config.setEmail_pass(email.get("pass").toString());
        Config.setEmail_host(email.get("host").toString());
        Config.setEmail_port(email.get("port").toString());
        Config.setEmail_protocol(email.get("protocol").toString());

        LinkedHashMap vnpy = (LinkedHashMap) ret.get("vnpy");
        Config.setVnpy_ip(vnpy.get("ip").toString());
        Config.setVnpy_port((Integer) vnpy.get("port"));
        Config.setVnpy_username(vnpy.get("username").toString());
        Config.setVnpy_password(vnpy.get("password").toString());

        LinkedHashMap kafka = (LinkedHashMap) ret.get("kafka");
        Config.setKafka_ip(kafka.get("ip").toString());
        Config.setKafka_port(kafka.get("port").toString());
        Config.setKafka_groupId(kafka.get("groupId").toString());
        String topic = kafka.get("topic").toString();
        int length = topic.length();
        String[] topicArray = topic.substring(1, length - 1).split(",");
        Config.setKafka_topic(topicArray);


    }

}