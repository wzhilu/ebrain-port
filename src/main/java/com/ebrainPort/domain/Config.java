package com.ebrainPort.domain;

public class Config {

    public static String getUrl_analysis() {
        return url_analysis;
    }

    public static void setUrl_analysis(String url_analysis) {
        Config.url_analysis = url_analysis;
    }

    public static String getUrl_strategy() {
        return url_strategy;
    }

    public static void setUrl_strategy(String url_strategy) {
        Config.url_strategy = url_strategy;
    }

    public static String getUrl_inform() {
        return url_inform;
    }

    public static void setUrl_inform(String url_inform) {
        Config.url_inform = url_inform;
    }

    public static String getEmail_account() {
        return email_account;
    }

    public static void setEmail_account(String email_account) {
        Config.email_account = email_account;
    }

    public static String getEmail_pass() {
        return email_pass;
    }

    public static void setEmail_pass(String email_pass) {
        Config.email_pass = email_pass;
    }

    public static String getEmail_host() {
        return email_host;
    }

    public static void setEmail_host(String email_host) {
        Config.email_host = email_host;
    }

    public static String getEmail_port() {
        return email_port;
    }

    public static void setEmail_port(String email_port) {
        Config.email_port = email_port;
    }

    public static String getEmail_protocol() {
        return email_protocol;
    }

    public static void setEmail_protocol(String email_protocol) {
        Config.email_protocol = email_protocol;
    }

    public static String getVnpy_ip() {
        return vnpy_ip;
    }

    public static void setVnpy_ip(String vnpy_ip) {
        Config.vnpy_ip = vnpy_ip;
    }

    public static int getVnpy_port() {
        return vnpy_port;
    }

    public static void setVnpy_port(int vnpy_port) {
        Config.vnpy_port = vnpy_port;
    }

    public static String getVnpy_username() {
        return vnpy_username;
    }

    public static void setVnpy_username(String vnpy_username) {
        Config.vnpy_username = vnpy_username;
    }

    public static String getVnpy_password() {
        return vnpy_password;
    }

    public static void setVnpy_password(String vnpy_password) {
        Config.vnpy_password = vnpy_password;
    }

    public static String getKafka_ip() {
        return kafka_ip;
    }

    public static void setKafka_ip(String kafka_ip) {
        Config.kafka_ip = kafka_ip;
    }

    public static String getKafka_port() {
        return kafka_port;
    }

    public static void setKafka_port(String kafka_port) {
        Config.kafka_port = kafka_port;
    }

    public static String getKafka_groupId() {
        return kafka_groupId;
    }

    public static void setKafka_groupId(String kafka_groupId) {
        Config.kafka_groupId = kafka_groupId;
    }

    public static String[] getKafka_topic() {
        return kafka_topic;
    }

    public static void setKafka_topic(String[] kafka_topic) {
        Config.kafka_topic = kafka_topic;
    }

    public static String getTime_test() {
        return time_test;
    }

    public static void setTime_test(String time_test) {
        Config.time_test = time_test;
    }

    public static String getTime_reality() {
        return time_reality;
    }

    public static void setTime_reality(String time_reality) {
        Config.time_reality = time_reality;
    }

    private static String url_analysis;
    private static String url_strategy;
    private static String url_inform;
    private static String email_account;
    private static String email_pass;
    private static String email_host;
    private static String email_port;
    private static String email_protocol;
    private  static String vnpy_ip;
    private  static int vnpy_port;
    private  static String vnpy_username;
    private  static String vnpy_password;
    private  static String kafka_ip;
    private  static String kafka_port;
    private  static String kafka_groupId;
    private  static String[] kafka_topic;
    private  static String time_test;
    private  static String time_reality;

}