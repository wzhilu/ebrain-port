package com.ebrainPort.utils;

import org.yaml.snakeyaml.DumperOptions;
import org.yaml.snakeyaml.Yaml;
import org.yaml.snakeyaml.error.YAMLException;

import java.io.*;
import java.util.LinkedHashMap;


/**
 * @author Relic
 * @desc 操作yml的工具类
 * @date 2019-01-29 20:03
 */
public class YmlUtils {

    private final static DumperOptions OPTIONS = new DumperOptions();
    private final static String OUTPATH = System.getProperty("user.dir") + File.separator + "config.yml";

    static {
        //将默认读取的方式设置为块状读取
        OPTIONS.setDefaultFlowStyle(DumperOptions.FlowStyle.BLOCK);
    }

    /**
     * 在目标文件中添加新的配置信息
     *
     * @param dest  需要添加信息的目标yml文件
     * @param key   添加的key值
     * @param value 添加的对象(如key下方还有链接则添加LinkedHashMap)
     * @author Relic
     * @title addIntoYml
     * @date 2019/1/29 20:52
     */
    public static void addIntoYml(File dest, String key, Object value) throws IOException {
        Yaml yaml = new Yaml(OPTIONS);
        //载入当前yml文件
        LinkedHashMap<String, Object> dataMap = yaml.load(new FileReader(dest));
        //如果yml内容为空,则会引发空指针异常,此处进行判断
        if (null == dataMap) {
            dataMap = new LinkedHashMap<>();
        }
        dataMap.put(key, value);
        //将数据重新写回文件
        yaml.dump(dataMap, new FileWriter(dest));
    }

    public static LinkedHashMap getYmlConfig(String root) {
        Yaml yaml = new Yaml();
        LinkedHashMap ret;
        try {
            InputStream in = new FileInputStream(new File(OUTPATH));
            ret = yaml.load(in);
            return (LinkedHashMap) ret.get(root);
        } catch (YAMLException | FileNotFoundException e) {
            ret = yaml.load(YmlUtils.class.getClassLoader().getResourceAsStream("config.yml"));
        }
        return (LinkedHashMap) ret.get(root);
    }

    /**
     * 从目标yml文件中读取出所指定key的值
     *
     * @param source 获取yml信息的文件
     * @param key    需要获取信息的key值
     * @return java.lang.Object
     * @author Relic
     * @title getFromYml
     * @date 2019/1/29 20:56
     */
    public static Object getFromYml(File source, String key) throws IOException {
        Yaml yaml = new Yaml(OPTIONS);
        //载入文件
        LinkedHashMap<String, Object> dataMap = yaml.load(new FileReader(source));
        //获取当前key下的值(如果存在多个节点,则value可能为map,自行判断)
        return dataMap.get(key);
    }

}