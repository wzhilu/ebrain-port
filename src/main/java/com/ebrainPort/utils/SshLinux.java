package com.ebrainPort.utils;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStreamWriter;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.nio.charset.StandardCharsets;

import ch.ethz.ssh2.ChannelCondition;
import ch.ethz.ssh2.Connection;
import ch.ethz.ssh2.Session;
import ch.ethz.ssh2.StreamGobbler;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class SshLinux {

    /**
     * 连接linux系统
     */

    private static final Logger logger = LoggerFactory.getLogger(SshLinux.class);
    private static final int TIMEOUT = 2000;
    private static final int RECONNECT_TIMES = 20;
    private static int waitTime;

    public static Connection openConnection(String host, int port, String userName, String password) throws Exception {

        Connection connection = connected(host, port);
        if (!connection.authenticateWithPassword(userName, password)) {
            throw new Exception("用户名密码错误");
        }
        logger.info("登陆成功!");

        return connection;
    }

    private static Connection connected(String host, int port) {

        Connection conn = new Connection(host, port);
        int connectTimes = 1;
        long waitTime = System.currentTimeMillis() + (long) SshLinux.TIMEOUT;
        do {
            try {
                conn.connect();
                break;
            } catch (IOException e) {
                logger.error("ssh连接到主机时出错", e);
                connectTimes++;
            }
        } while (System.currentTimeMillis() < waitTime && RECONNECT_TIMES <= connectTimes);
        return conn;
    }

    public static String execmd(Connection connection, String command) throws Exception {
        Session session = connection.openSession();
        session.requestPTY("");
        session.startShell();
        if (command.equals("")) {
            logger.info("执行空指令");
            return null;
        }
        try (PrintWriter out = new PrintWriter(new OutputStreamWriter(session.getStdin(),
                StandardCharsets.UTF_8))) {
            out.println(command);
            out.println("exit");
            out.flush();
            return receiveMsg(session);
        }
    }


    private static String receiveMsg(Session session) throws InterruptedException {
        StringBuilder resultOut = new StringBuilder();
        Thread.sleep(10);
        try (InputStream stdout = new StreamGobbler(session.getStdout()); InputStream stderr = new StreamGobbler(session.getStderr())) {
            int conditions = session.waitForCondition(ChannelCondition.STDOUT_DATA | ChannelCondition.STDERR_DATA | ChannelCondition.EOF, waitTime);

            if ((conditions & ChannelCondition.TIMEOUT) != 0) {
                logger.error("获取session数据超时");
                throw new IOException("获取打印数据超时");
            }
            if ((conditions & ChannelCondition.EOF) != 0) {
                logger.error("无数据可读");
            }
            BufferedReader stdoutReader = new BufferedReader(new InputStreamReader(stdout));
            BufferedReader stderrReader = new BufferedReader(new InputStreamReader(stderr));
            String line;
            int count = 1;
            while ((line = stdoutReader.readLine()) != null) {
                if (line.contains("#")) {
                    count--;
                }
                if (count < 1 && !line.contains("#") && !line.contains("$") && !line.startsWith("<") && !line.startsWith("TERM") && !line.endsWith("No such file or directory")) {
                    resultOut.append(line).append("\n");
//                    System.out.println(line);
                }
            }
            while ((line = stderrReader.readLine()) != null) {
                System.out.println(line);
            }
        } catch (IOException e) {
            e.printStackTrace(System.err);
            System.exit(2);
        } finally {
            session.close();
        }
        return resultOut.toString().trim();
    }

}









