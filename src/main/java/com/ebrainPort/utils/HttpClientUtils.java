package com.ebrainPort.utils;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InterruptedIOException;
import java.net.UnknownHostException;
import java.security.KeyManagementException;
import java.security.KeyStore;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.net.ssl.*;

import org.apache.commons.collections.MapUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.http.Consts;
import org.apache.http.Header;
import org.apache.http.HeaderIterator;
import org.apache.http.HttpEntity;
import org.apache.http.HttpEntityEnclosingRequest;
import org.apache.http.HttpRequest;
import org.apache.http.HttpStatus;
import org.apache.http.NameValuePair;
import org.apache.http.NoHttpResponseException;
import org.apache.http.ParseException;
import org.apache.http.StatusLine;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.CookieStore;
import org.apache.http.client.HttpRequestRetryHandler;
import org.apache.http.client.config.CookieSpecs;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.entity.GzipDecompressingEntity;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpRequestBase;
import org.apache.http.client.protocol.HttpClientContext;
import org.apache.http.config.Registry;
import org.apache.http.config.RegistryBuilder;
import org.apache.http.config.SocketConfig;
import org.apache.http.conn.ConnectTimeoutException;
import org.apache.http.conn.socket.ConnectionSocketFactory;
import org.apache.http.conn.socket.PlainConnectionSocketFactory;
import org.apache.http.conn.ssl.NoopHostnameVerifier;
import org.apache.http.conn.ssl.SSLConnectionSocketFactory;
import org.apache.http.conn.ssl.TrustSelfSignedStrategy;
import org.apache.http.conn.ssl.TrustStrategy;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.BasicCookieStore;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.impl.conn.PoolingHttpClientConnectionManager;
import org.apache.http.impl.cookie.BasicClientCookie;
import org.apache.http.message.BasicHeader;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.protocol.HttpContext;
import org.apache.http.ssl.SSLContextBuilder;
import org.apache.http.ssl.SSLContexts;
import org.apache.http.util.EntityUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


public class HttpClientUtils {
    private static final Logger logger = LoggerFactory.getLogger(HttpClientUtils.class);
    private static final String GZIP = "gzip";
    private static final String HTTP = "http";
    private static final String HTTPS = "https";
    private static final String CHAR_SET = "UTF-8";

    private String InetAddressStr;
    private int InetPort;

    private static int MAX_CONNECTION_NUM = 400;
    private static int MAX_PER_ROUTE = 100;
    private static int DEFAULT_MAX_PER_ROUTE = 40;

    private static int SOCKET_TIME_OUT = 60000;
    private static int SO_TIME_OUT = 60000;
    private static int SO_LINGER = 60;
    private static int CONNECT_REQUEST_TIME_OUT = 60000;
    private static int SERVER_RESPONSE_TIME_OUT = 2000;
    private static int CONNECT_TIME_OUT = 60000;
    private static int EXECUTION_COUNT = 5;

    private static RequestConfig requestConfig;
    private static CloseableHttpClient httpClient;

    private static SSLConnectionSocketFactory sslsf;
    private static PoolingHttpClientConnectionManager cm;

    private final static Object syncLock = new Object();

    private static HttpRequestRetryHandler httpRequestRetryHandler;
    private static CookieStore cookieStore;
    private static String keyStoreFilePath;
    private static String keyStorePass;
    private static String trustStoreFilePath;
    private static String trustStorePass;

    static {
        requestConfig = RequestConfig.custom().setCookieSpec(CookieSpecs.STANDARD_STRICT).build();
        cookieStore = new BasicCookieStore();
        BasicClientCookie cookie = new BasicClientCookie("sessionID", "######");
        cookie.setDomain("#####");
        cookie.setPath("/");
        cookieStore.addCookie(cookie);

        httpRequestRetryHandler = (exception, executionCount, context) -> {
            if (executionCount >= EXECUTION_COUNT) {
                return false;
            }
            if (exception instanceof NoHttpResponseException) {
                return true;
            }
            if (exception instanceof SSLHandshakeException) {
                return false;
            }
            if (exception instanceof InterruptedIOException) {
                return false;
            }
            if (exception instanceof UnknownHostException) {
                return false;
            }
            if (exception instanceof SSLException) {
                return false;
            }

            HttpClientContext clientContext = HttpClientContext.adapt(context);
            HttpRequest request = clientContext.getRequest();
            return !(request instanceof HttpEntityEnclosingRequest);
        };


        System.out.println("int conection pool...");

        try {
            SSLContextBuilder builder = new SSLContextBuilder();
            builder.loadTrustMaterial(null, new TrustStrategy() {
                @Override
                public boolean isTrusted(X509Certificate[] x509Certificates, String s) {
                    return true;
                }
            });

            sslsf = new SSLConnectionSocketFactory(builder.build(),
                    new String[]{"SSLv2Hello", "SSLv3", "TLSv1", "TLSV1.2"},
                    null, NoopHostnameVerifier.INSTANCE);

            Registry<ConnectionSocketFactory> registry = RegistryBuilder.<ConnectionSocketFactory>create()
                    .register(HTTP, PlainConnectionSocketFactory.INSTANCE)
                    .register(HTTPS, sslsf)
                    .build();

            cm = new PoolingHttpClientConnectionManager(registry);
            cm.setMaxTotal(MAX_CONNECTION_NUM);
            cm.setDefaultMaxPerRoute(DEFAULT_MAX_PER_ROUTE);
            SocketConfig socketConfig = SocketConfig.custom()
                    .setTcpNoDelay(true)
                    .setSoReuseAddress(true)
                    .setSoTimeout(SO_TIME_OUT)
                    .setSoLinger(SO_LINGER)
                    .setSoKeepAlive(true)
                    .build();

            cm.setDefaultSocketConfig(socketConfig);
            getHttpClient();

        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    private static void initKeyStore(SSLContextBuilder builder) throws IOException {
        try (InputStream ksis = new FileInputStream(new File(keyStoreFilePath)); InputStream tsis = new FileInputStream(new File(trustStoreFilePath))) {

            KeyStore ks = KeyStore.getInstance("PKCS12");
            ks.load(ksis, keyStorePass.toCharArray());

            KeyStore ts = KeyStore.getInstance("JKS");
            ts.load(tsis, trustStorePass.toCharArray());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private static CloseableHttpClient getHttpClient() {
        if (httpClient == null) {
            synchronized (syncLock) {
                if (httpClient == null) {
                    httpClient = HttpClients.custom()
                            .setConnectionManager(cm)
//                            .setProxy(new HttpHost("myproxy", 8080))
                            .setDefaultCookieStore(cookieStore)
                            .setDefaultRequestConfig(requestConfig)
                            .setSSLSocketFactory(sslsf)
                            .setRetryHandler(httpRequestRetryHandler)
                            .setConnectionManagerShared(true)
                            .build();
                }
            }
        }
        return httpClient;
    }

    private static void requestConfig(HttpRequestBase httpget) {
        RequestConfig requestConfig = RequestConfig.custom()
                .setSocketTimeout(SOCKET_TIME_OUT)
                .setConnectTimeout(CONNECT_TIME_OUT)
                .setConnectionRequestTimeout(CONNECT_REQUEST_TIME_OUT)
//                .setProxy(new HttpHost("myotherproxy", 8080))
                .build();
        httpget.setConfig(requestConfig);
    }

    public static Map<String, String> getCookie(String url) {
//        getHttpClient();
        HttpGet httpGet = new HttpGet(url);
        CloseableHttpResponse response = null;
        try {
            response = httpClient.execute(httpGet);
            Header[] headers = response.getAllHeaders();
            Map<String, String> cookies = new HashMap<>();
            for (Header header : headers) {
                cookies.put(header.getName(), header.getValue());
            }
            return cookies;
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            closeResponse(response);
            closeHttpClient(httpClient);
        }
        return null;
    }

    public static String post(String url, Map<String, String> header, Map<String, String> param, HttpEntity entity) throws Exception {
        String result = "";
        CloseableHttpResponse response = null;
        try {
            HttpPost httpPost = new HttpPost(url);
            if (MapUtils.isNotEmpty(header)) {
                for (Map.Entry<String, String> entry : header.entrySet()) {
                    httpPost.addHeader(entry.getKey(), entry.getValue());
                }
            }

            if (MapUtils.isNotEmpty(param)) {
                List<NameValuePair> formparams = new ArrayList<>();
                for (Map.Entry<String, String> entry : param.entrySet()) {
                    formparams.add(new BasicNameValuePair(entry.getKey(), entry.getValue()));
                }
                UrlEncodedFormEntity urlEncodedFormEntity = new UrlEncodedFormEntity(formparams, Consts.UTF_8);
                httpPost.setEntity(urlEncodedFormEntity);
            }
            if (entity != null) {
                httpPost.setEntity(entity);
            }
            requestConfig(httpPost);
            response = httpClient.execute(httpPost);
            int statusCode = response.getStatusLine().getStatusCode();
            if (statusCode == HttpStatus.SC_OK) {
                HttpEntity resEntity = response.getEntity();
                result = EntityUtils.toString(resEntity);
            } else {
                readHttpResponse(response);
            }
        } finally {
            closeResponse(response);
//            closeHttpClient(httpClient);
        }
        return result;
    }

    private static String readHttpResponse(CloseableHttpResponse httpResponse) throws ParseException, IOException {
        StringBuilder builder = new StringBuilder();
        HttpEntity entity = httpResponse.getEntity();
        builder.append("status:").append(httpResponse.getStatusLine());
        builder.append("header:");
        HeaderIterator iterator = httpResponse.headerIterator();
        while (iterator.hasNext()) {
            builder.append("\t").append(iterator.next());
        }
        if (entity != null) {
            String responseString = EntityUtils.toString(entity);
            builder.append("response length:").append(responseString.length());
            builder.append("response content:").append(responseString.replace("\r\n", ""));
        }
        return builder.toString();
    }

    public static HttpEntity postJson(String url, Map<String, Object> headers, String data) {
        HttpPost request = new HttpPost(url);
        if (headers != null && !headers.isEmpty()) {
            setHeaders(headers, request);
        }
        CloseableHttpResponse response = null;
        try {
            request.setEntity(new StringEntity(data, ContentType.create("application/json", CHAR_SET)));
            response = httpClient.execute(request);
            return response.getEntity();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            closeResponse(response);
        }
        return null;
    }

    public static String postJsonToString(String url, Map<String, Object> headers, String data) {
        HttpPost request = new HttpPost(url);
        if (headers != null && !headers.isEmpty()) {
            setHeaders(headers, request);
        }
        CloseableHttpResponse response = null;
        try {
            request.setEntity(new StringEntity(data, ContentType.create("application/json", CHAR_SET)));
            response = httpClient.execute(request);
            HttpEntity entity = response.getEntity();
            return EntityUtils.toString(entity);
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            closeResponse(response);
        }
        return null;
    }

    private static void setHeaders(Map<String, Object> headers, HttpRequest request) {
        for (Map.Entry<String, Object> entry : headers.entrySet()) {
            if (!entry.getKey().equals("Cookie")) {
                request.addHeader(entry.getKey(), (String) entry.getValue());
            } else {
                Map<String, Object> Cookies = new HashMap<>();
                Cookies.put(entry.getKey(), entry.getValue());
                for (Map.Entry<String, Object> entry1 : Cookies.entrySet()) {
                    request.addHeader(new BasicHeader("Cookie", (String) entry1.getValue()));
                }
            }
        }
    }

    public static HttpEntity postForm(String url, Map<String, Object> headers, List<NameValuePair> data) {
        CloseableHttpClient httpClient = getHttpClient();
        HttpPost request = new HttpPost(url);
        requestConfig(request);
        if (headers != null && !headers.isEmpty()) {
            setHeaders(headers, request);
        }
        CloseableHttpResponse response = null;
        try {
            UrlEncodedFormEntity uefEntity = new UrlEncodedFormEntity(data, CHAR_SET);
            request.setEntity(uefEntity);
            response = httpClient.execute(request);
            return response.getEntity();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            closeResponse(response);
        }
        return null;
    }

    private static String assemblyGetRequestUrlAndParams(String url, Map<String, String> params) {
        if (StringUtils.isBlank(url)) {
            logger.error("Request URL Is Not Null");
            return null;
        }
        StringBuilder builder = new StringBuilder(url);
        if (params != null && !params.isEmpty()) {
            if (!url.contains("?")) {
                builder.append("?");
            }
            for (Map.Entry<String, String> entry : params.entrySet()) {
                if (url.indexOf("?") > 0) {
                    builder.append("&").append(entry.getKey()).append("=").append(entry.getValue());
                } else {
                    builder.append(entry.getKey()).append("=").append(entry.getValue());
                }
            }
        }
        return builder.toString();
    }

    private static CloseableHttpResponse getResponse(String url, Map<String, String> headers) {
//        getHttpClient();
        HttpGet httpGet = new HttpGet(url);
        if (headers != null && !headers.isEmpty()) {
            for (Map.Entry<String, String> entry : headers.entrySet()) {
                httpGet.setHeader(entry.getKey(), entry.getValue());
            }
        }
//        requestConfig(httpGet);
        CloseableHttpResponse response = null;
        try {
            response = httpClient.execute(httpGet);
            if (response.getStatusLine().getStatusCode() != HttpStatus.SC_OK) {
                httpGet.abort();
                return response;
            }
        } catch (ClientProtocolException e) {
            httpGet.abort();
            logger.error(e.getMessage());
            e.printStackTrace();
            return null;
        } catch (IOException e) {
            httpGet.abort();
            logger.error(e.getMessage());
            e.printStackTrace();
            return response;
        }
        return response;
    }

    public static HttpEntity get(String url, Map<String, String> params, Map<String, String> headers) {
        String tempUrl = assemblyGetRequestUrlAndParams(url, params);
        CloseableHttpResponse response = getResponse(tempUrl, headers);
        if (response == null) {
            logger.error("CloseableHttpResponse Object Is Not Null");
            return null;
        }
        HttpEntity entity = response.getEntity();
        try {
            StatusLine statusLine = response.getStatusLine();
            int responseCode = statusLine.getStatusCode();
            if (responseCode == 200) {
                System.out.println("响应成功!");
                System.out.println("信息----------------------------------------");
                System.out.println(response.getStatusLine());
                System.out.println("----------------------------------------");
            } else {
                System.out.println("响应失败!");
                System.out.println("错误信息----------------------------------------");
                System.out.println(response.getStatusLine());
                System.out.println("----------------------------------------");
            }
            EntityUtils.consume(entity);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            closeResponse(response);
//            closeHttpClient(httpClient);
        }
        return entity;
    }

    public static String getString(String url, Map<String, String> params, Map<String, String> headers) {
        String tempUrl = assemblyGetRequestUrlAndParams(url, params);
        System.out.println(tempUrl);
        CloseableHttpResponse response = getResponse(tempUrl, headers);
        if (response == null) {
            logger.error("CloseableHttpResponse Object Is Not Null");
            return null;
        }
        HttpEntity entity = response.getEntity();
        if (entity != null) {
            try {
                if (entity.getContentEncoding() != null && GZIP.equalsIgnoreCase(entity.getContentEncoding().getValue())) {
                    entity = new GzipDecompressingEntity(entity);
                }
                String temp = EntityUtils.toString(entity);
                EntityUtils.consume(entity);
                return temp;
            } catch (ParseException | IOException e) {
                e.printStackTrace();
            } finally {
                closeResponse(response);
//                closeHttpClient(httpClient);
            }
        }
        return null;
    }

    private static void closeResponse(CloseableHttpResponse response) {
        if (response != null) {
            try {
                response.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    private static void closeHttpClient(CloseableHttpClient httpClient) {
        if (httpClient != null) {
            try {
                httpClient.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    private static class SingletonHttpClientHolder {
        private static final CloseableHttpClient closeableHttpClient = HttpClients.custom()
                .setConnectionManager(cm)
//                .setProxy(new HttpHost("myproxy", 8080))
                .setDefaultCookieStore(cookieStore)
                .setDefaultRequestConfig(requestConfig)
                .setSSLSocketFactory(sslsf)
                .setRetryHandler(httpRequestRetryHandler)
                .setConnectionManagerShared(true)
                .build();

    }

    public static CloseableHttpClient getConnectionInsatance() {
        return SingletonHttpClientHolder.closeableHttpClient;
    }

    private static CloseableHttpClient iniHttpClient(String urlParam, String cerpath) throws NoSuchAlgorithmException, KeyManagementException {
        CloseableHttpClient httpclient = null;
        urlParam = urlParam.trim();
        if (urlParam.startsWith("http://")) {
            httpclient = HttpClients.createDefault();
        } else if (urlParam.startsWith("https://")) {
            //采用绕过验证的方式处理https请求
            SSLContext sslContext = null;
            if (null == cerpath || "".equals(cerpath.trim())) {
//                LogUtil.APP.info("开始构建HTTPS单向认证请求...");
                TrustManager[] trustManagers = {new MyX509TrustManager()};
                sslContext = SSLContext.getInstance("TLS");
                sslContext.init(null, trustManagers, new SecureRandom());
            } else {
//                LogUtil.APP.info("开始构建HTTPS双向认证请求...");
                String[] strcerpath = cerpath.split(";", -1);
                sslContext = sslContextKeyStore(strcerpath[0], strcerpath[1]);
            }

            // 设置协议http和https对应的处理socket链接工厂的对象
            Registry<ConnectionSocketFactory> socketFactoryRegistry = RegistryBuilder.<ConnectionSocketFactory>create()
                    .register("http", PlainConnectionSocketFactory.INSTANCE)
                    // 正常的SSL连接会验证所有证书信息
                    // .register("https", new SSLConnectionSocketFactory(sslContext)).build();
                    // 忽略域名验证
                    .register("https", new SSLConnectionSocketFactory(sslContext, NoopHostnameVerifier.INSTANCE))
                    .build();
            PoolingHttpClientConnectionManager connManager = new PoolingHttpClientConnectionManager(socketFactoryRegistry);
            connManager.setDefaultMaxPerRoute(1);
            //创建自定义的httpclient对象
            httpclient = HttpClients.custom().setConnectionManager(connManager).build();
        } else {
            httpclient = HttpClients.createDefault();
        }
        return httpclient;
    }

    private static SSLContext sslContextKeyStore(String keyStorePath, String keyStorepass) {
        SSLContext sslContext = null;
        FileInputStream instream = null;
        KeyStore trustStore = null;
//        LogUtil.APP.info("证书路径:{}  密钥:{}", keyStorePath, keyStorepass);
        try {
            trustStore = KeyStore.getInstance(KeyStore.getDefaultType());
//            LogUtil.APP.info("开始读取证书文件流...");
            instream = new FileInputStream(new File(keyStorePath));
//            LogUtil.APP.info("开始设置证书以及密钥...");
            trustStore.load(instream, keyStorepass.toCharArray());
            // 相信自己的CA和所有自签名的证书
            sslContext = SSLContexts.custom().loadTrustMaterial(trustStore, new TrustSelfSignedStrategy()).build();
            // 构造 javax.net.ssl.TrustManager 对象
            TrustManagerFactory tmf = TrustManagerFactory.getInstance("SunX509", "SunJSSE");
            tmf.init(trustStore);
            TrustManager[] tms = tmf.getTrustManagers();
            // 使用构造好的 TrustManager 访问相应的 https 站点
            sslContext = SSLContext.getInstance("SSL", "SunJSSE");
            sslContext.init(null, tms, new java.security.SecureRandom());
        } catch (Exception e) {
//            LogUtil.APP.error("设置信任自签名证书出现异常，请检查！", e);
        } finally {
            try {
                instream.close();
            } catch (IOException e) {
//                LogUtil.APP.error("设置信任自签名证书后关闭instream流出现异常，请检查！", e);
            }
        }
        return sslContext;
    }

}