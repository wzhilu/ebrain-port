package com.ebrainPort.utils;

import java.io.IOException;
import java.security.GeneralSecurityException;
import java.util.Date;
import java.util.Properties;

import javax.mail.Authenticator;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.*;

import com.ebrainPort.domain.Config;
import com.sun.mail.util.MailSSLSocketFactory;


public class SendEmailUtil {

    static class MyAuthenricator extends Authenticator {
        String u = null;
        String p = null;

        MyAuthenricator(String u, String p) {
            this.u = u;
            this.p = p;
        }

        @Override
        protected PasswordAuthentication getPasswordAuthentication() {
            return new PasswordAuthentication(u, p);
        }
    }

    private String to;    //收件人
    private String cc;
    private String subject;    //主题
    private String content;    //内容
    private String fileStr;    //附件路径

    public SendEmailUtil(String to, String cc, String subject, String content, String fileStr) {
        this.to = to;
        this.cc = cc;
        this.subject = subject;
        this.content = content;
        this.fileStr = fileStr;
    }

    public void send() {
        Properties prop = new Properties();
        //协议
        prop.setProperty("mail.transport.protocol", Config.getEmail_protocol());
        //服务器
        prop.setProperty("mail.smtp.host", Config.getEmail_host());
        //端口
        prop.setProperty("mail.smtp.port", Config.getEmail_port());
        //使用smtp身份验证
        prop.setProperty("mail.smtp.auth", "true");
        //使用SSL，企业邮箱必需！
        //开启安全协议
        MailSSLSocketFactory sf = null;
        try {
            sf = new MailSSLSocketFactory();
            sf.setTrustAllHosts(true);
        } catch (GeneralSecurityException e1) {
            e1.printStackTrace();
        }
        prop.put("mail.smtp.ssl.enable", "true");
        prop.put("mail.smtp.ssl.socketFactory", sf);

        Session session = Session.getDefaultInstance(prop, new MyAuthenricator(Config.getEmail_account(), Config.getEmail_pass()));
//        session.setDebug(true);
        MimeMessage mimeMessage = new MimeMessage(session);
        try {
            //发件人
            mimeMessage.setFrom(new InternetAddress(Config.getEmail_account(), "wangyugang"));        //可以设置发件人的别名
            //mimeMessage.setFrom(new InternetAddress(account));    //如果不需要就省略
            //收件人
            mimeMessage.addRecipients(Message.RecipientType.TO, InternetAddress.parse(to));
//            抄送人
            mimeMessage.addRecipients(Message.RecipientType.CC, InternetAddress.parse(cc));
            //主题
            mimeMessage.setSubject(subject);
            //时间
            mimeMessage.setSentDate(new Date());
            //容器类，可以包含多个MimeBodyPart对象
            Multipart mp = new MimeMultipart();

            //MimeBodyPart可以包装文本，图片，附件
            MimeBodyPart body = new MimeBodyPart();
            //HTML正文
            body.setContent(content, "text/html; charset=UTF-8");
            mp.addBodyPart(body);

            //添加图片&附件
            if (!fileStr.equals("nothing")) {
                MimeBodyPart bodyExtra = new MimeBodyPart();
                bodyExtra.attachFile(fileStr);
                bodyExtra.setFileName(MimeUtility.encodeText(MyUtil.getFilename(fileStr)));
                mp.addBodyPart(bodyExtra);
            }

            //设置邮件内容
            mimeMessage.setContent(mp);
            //仅仅发送文本
            //mimeMessage.setText(content);
            mimeMessage.saveChanges();
            Transport.send(mimeMessage);
        } catch (MessagingException | IOException e) {
            e.printStackTrace();
        }
    }


}