package com.ebrainPort.utils;

import java.io.File;
import java.time.LocalDate;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;


public class MyUtil {

    private static final String SEPARATOR = File.separator;

    public static String obtainOutputPath() {
        String projectRootPath = System.getProperty("user.dir");
        return projectRootPath + SEPARATOR + "result" + SEPARATOR;
    }

    static String getFilename(String filePath) {
        return filePath.substring(filePath.lastIndexOf(SEPARATOR) + 1);
    }

    public static String getNowTime() {
        DateTimeFormatter dtf = DateTimeFormatter.ofPattern("HH:mm");
        return dtf.format(LocalTime.now());
    }

    public static String getNowDate() {
        DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyyMMdd");
        return dtf.format(LocalDate.now());
    }

    public static List<String> myMatcher(String originalData, String regex) {
        Pattern pattern = Pattern.compile(regex);
        Matcher matcher = pattern.matcher(originalData);
        List<String> list = new ArrayList<>();
        if (matcher.find()) {
            int groupCount = matcher.groupCount();
            for (int i = 1; i <= groupCount; i++) {
                list.add(matcher.group(i));
            }
        }
        return list;
    }


}

